import VideoSuggestion from '../models/video-suggestion.model.js';
import Question from '../models/question.model.js';
import { logger } from '../helper/logger.js';

export default (app, router, io, admin) => {

  /**
   * @api {get} /api/video-suggestion Get video suggestions
   * @apiName ListVideoSuggestions
   * @apiGroup VideoSuggestion
   *
   * @apiSuccess {Object[]} entry             List of suggestions
   * @apiSuccess {Number}   entry.ended       End time
   * @apiSuccess {Number}   entry.started     Start time
   * @apiSuccess {String}   entry.videoUrl    Youtube url
   */
  router.get('/api/video-suggestion', admin, async (req, res) => {
    try {
      const result = await VideoSuggestion
        .find()
        .sort({
          createdAt: 'asc'
        })
        .populate('user', 'name username avatarImageToken')
        .populate('question', 'text videoUrl');

      res.json(result);
    } catch (err) {
      logger.error(req.url, 'Error', err);
      res.status(400).send(err);
    }
  });

  /**
   * @api {post} /api/video-suggestion Add new video-suggestion
   * @apiName AddVideoSuggestion
   * @apiGroup VideoSuggestion
   */
  router.post('/api/video-suggestion/:id', admin, async (req, res) => {
    try {
      const result = await VideoSuggestion
        .findOne({ _id: req.params.id })
        .exec();

      const question = await Question
        .findOne({ _id: result.question })
        .exec();

      question.videoUrl = result.videoUrl;
      question.videoUrlSuggestions =[];

      await question.save();
      await result.remove();

      io.sockets.emit('question', 0);

      res.sendStatus(201);
    } catch (err) {
      logger.error(req.url, err);
      res
        .status(err.name === 'ValidationError' ? 400 : 500)
        .send(err);
    }
  });

  /**
   * @api {delete} /api/video-suggestion/:id Deletes video-suggestion
   * @apiName DeleteVideoSuggestion
   * @apiGroup VideoSuggestion
   */
  router.delete('/api/video-suggestion/:id', admin, async (req, res) => {
    try {
      const result = await VideoSuggestion
        .find({ _id: req.params.id })
        .remove()
        .exec();

      res.json(result);
    } catch (err) {
      logger.error(req.url, err);
      res.send(err);
    }
  });
};
