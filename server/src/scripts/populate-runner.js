require('babel-register');
require('babel-polyfill');

const tables = ['user','appointment','message','question', 'commitment'];
// run es6 file
require('./populate-dev')(tables);
