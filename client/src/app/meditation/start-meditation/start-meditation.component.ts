import { Component, ChangeDetectionStrategy } from '@angular/core';
import { Store } from '@ngrx/store';
import { selectPosting } from '../reducers/meditation.reducers';
import { PostMeditation } from '../actions/meditation.actions';
import { Observable } from 'rxjs';
import { AppState } from 'app/reducers';
import { MatBottomSheetRef } from '@angular/material';

@Component({
  selector: 'app-start-meditation',
  templateUrl: './start-meditation.component.html',
  styleUrls: ['./start-meditation.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class StartMeditationComponent {
  walking = window.localStorage.getItem('lastWalking');
  sitting = window.localStorage.getItem('lastSitting');

  posting$: Observable<boolean>;

  constructor(private store: Store<AppState>, public sheetRef: MatBottomSheetRef) {
    this.posting$ = this.store.select(selectPosting);
  }

  startMeditation() {
    const walking = parseInt(this.walking, 10);
    const sitting = parseInt(this.sitting, 10);

    if (!walking && !sitting) {
      return;
    }

    this.store.dispatch(new PostMeditation({ walking, sitting }));
  }
}
