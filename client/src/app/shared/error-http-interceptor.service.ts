import {
  HttpInterceptor,
  HttpRequest,
  HttpHandler,
  HttpSentEvent,
  HttpHeaderResponse,
  HttpProgressEvent,
  HttpResponse,
  HttpUserEvent,
  HttpErrorResponse
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, take, tap } from '../../../node_modules/rxjs/operators';
import { Store } from '../../../node_modules/@ngrx/store';
import { AppState } from '../reducers';
import { SetConnectionStatus } from '../actions/global.actions';
import { selectLastConnectionStatus } from '../reducers/global.reducers';
import { MatSnackBar } from '@angular/material';
import { LogoutDone } from '../auth/actions/auth.actions';

@Injectable()
export class ErrorHttpInterceptorService implements HttpInterceptor {

  constructor(private snackbar: MatSnackBar, private store: Store<AppState>) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpSentEvent
    | HttpHeaderResponse | HttpProgressEvent | HttpResponse<any> | HttpUserEvent<any>> {
      return next.handle(req).pipe(
        catchError((error) => {
          if (!(error instanceof HttpErrorResponse)) {
            return;
          }

          this.store.select(selectLastConnectionStatus).pipe(
            take(1)
          ).subscribe(lastStatus => {
            if (lastStatus === error.status) {
              return;
            }

            switch (error.status) {
              case 0:
                this.snackbar.open(
                  `We're unable to contact the server. Either you have no internet connection
                  or the server is offline at the moment. Please try again later.`,
                  null, {
                    duration: 5000
                  }
                );
                break;
              case 401:
                if (error.error === 'invalid token') {
                  this.snackbar.open(
                    `Your session has expired. Please login again.`,
                    null,
                    { duration: 5000 }
                  );

                  // dispatch LogoutDone() rather than Logout() because this request
                  // would not work with an invalid token
                  this.store.dispatch(new LogoutDone());
                }
                break;
              case 503:
                this.snackbar.open(
                  `The server currently is unavailable. Please try again later.`,
                  null,
                  { duration: 5000 }
                );
            }
          });

          this.store.dispatch(new SetConnectionStatus(error.status));

          return throwError(error);
        }
      ),
      tap((resp: HttpResponse<any>) => {
        if (resp.status !== null && typeof resp.status !== 'undefined') {
          this.store.dispatch(new SetConnectionStatus(resp.status));
        }
      })
    );
  }
}
