import { Injectable } from '@angular/core';
import { ApiConfig } from '../../api.config';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class LiveService {

  public constructor(private http: HttpClient) {
  }

  public getLiveData(): Observable<any> {
    return this.http.get(ApiConfig.url + '/api/live');
  }
}
