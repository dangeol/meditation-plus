import { Component, OnInit, OnDestroy, HostListener } from '@angular/core';
import { AppointmentService } from './appointment.service';
import { ActivatedRoute } from '@angular/router';
import { UserService } from '../user/user.service';
import * as moment from 'moment-timezone';
import { filter, take, concatMap } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import { SetTitle, ThrowError } from '../actions/global.actions';
import { LoadMeeting, InitiateMeeting } from './actions/appointment.actions';
import { AppState } from '../reducers';
import { Observable, Subscription, interval } from 'rxjs';
import { selectId, selectAdmin } from '../auth/reducders/auth.reducers';
import {
  selectMeeting,
  selectMeetingInitiateSuccess,
  selectMeetingInitiatePending,
  selectMeetingInitiateError
} from './reducers/appointment.reducer';
import { DialogService } from '../dialog/dialog.service';
import { MatSnackBar } from '@angular/material';
import { Meeting } from './appointment';
import { get as gv, keys } from 'lodash';

@Component({
  selector: 'appointment',
  templateUrl: './appointment.component.html',
  styleUrls: [
    './appointment.component.styl'
  ]
})
export class AppointmentComponent implements OnInit, OnDestroy {

  appointments: object[];
  appointmentsByHour: object;
  appointmentHours: string[];
  appointmentsByDay: object;
  ownAppointment: object = null;

  appointmentSocket;

  loadedInitially = false;
  currentTab = 'table';
  listViewShowBookable = true;

  profileTz = '';
  browserTz = moment.tz.guess();

  profile;

  userId$: Observable<string>;
  admin$: Observable<boolean>;
  meeting$: Observable<Meeting>;
  meetingInitiateSuccess$: Observable<boolean>;
  meetingInitiatePending$: Observable<boolean>;
  meetingInitiateError$: Observable<string>;

  checkMeetingInterval: Subscription;

  constructor(
    private appointmentService: AppointmentService,
    private route: ActivatedRoute,
    private userService: UserService,
    private dialog: DialogService,
    private snackbar: MatSnackBar,
    private store: Store<AppState>
  ) {
    store.dispatch(new SetTitle('Schedule'));
    this.userId$ = store.select(selectId);
    this.admin$ = store.select(selectAdmin);
    this.meeting$ = store.select(selectMeeting);
    this.meetingInitiateSuccess$ = store.select(selectMeetingInitiateSuccess);
    this.meetingInitiatePending$ = store.select(selectMeetingInitiatePending);
    this.meetingInitiateError$ = store.select(selectMeetingInitiateError);

    this.route.params
      .pipe(
        filter(res => res.hasOwnProperty('tab'))
      )
      .subscribe(res => this.currentTab = (<any>res).tab);

    this.store.dispatch(new LoadMeeting());
    this.meeting$.subscribe(meeting => {
      if (meeting) {
        if (this.checkMeetingInterval) {
          this.checkMeetingInterval.unsubscribe();
          this.checkMeetingInterval = null;
        }
      } else if (!this.checkMeetingInterval) {
        this.checkMeetingInterval = interval(4000)
          .subscribe(() => this.store.dispatch(new LoadMeeting()));
      }
    });
  }

  initiateMeeting(): void {
    this.store.dispatch(new InitiateMeeting());
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.currentTab = event.target.innerWidth > 800 ? 'table' : 'list';
  }

  /**
   * Method for querying appointments
   */
  loadAppointments(): void {
    this.ownAppointment = null;
    this.appointmentService.getAll()
      .subscribe(
        res => {
          this.appointments = res.appointments || [];
          this.ownAppointment = res.ownAppointment || null;
          this.getHoursForLocalTime();
        },
        null,
        () => this.loadedInitially = true
      );
  }

  /**
   * Get local timezone either from profile or browser
   */
  getUserTz(): string {
    let userTz = '';

    if (moment.tz.zone(this.profileTz)) {
      userTz = this.profileTz;
    } else if (moment.tz.zone(this.browserTz)) {
      userTz = this.browserTz;
    }

    return userTz;
  }

  /**
   * Get local hours which will be rows in the table
   */
  getHoursForLocalTime(): void {
    if (!this.appointments) {
      return;
    }

    const userTz = this.getUserTz();

    this.appointmentsByHour = {};
    this.appointmentsByDay = {};

    this.appointments.map((appointment) => {
      const localTime = moment(appointment['nextDate']).tz(userTz);
      const localHour = localTime.format('HH:mm z');
      const localDay = localTime.weekday();

      if (!gv(this.appointmentsByHour, localHour, null)) {
        this.appointmentsByHour[localHour] = [];
      }

      if (!gv(this.appointmentsByDay, localDay, null)) {
        this.appointmentsByDay[localDay] = [];
      }

      this.appointmentsByHour[localHour].push({
        localHour,
        localDay,
        ...appointment
      });

      this.appointmentsByDay[localDay].push({
        localHour,
        localDay,
        ...appointment
      });
    });

    this.appointmentHours = keys(this.appointmentsByHour).sort();
  }

  /**
   * Registration handling
   */
  toggleRegistration(evt, appointment) {
    evt.preventDefault();

    this.userId$.pipe(
      take(1),
      filter(id => !appointment.user || appointment.user._id === id),
      concatMap(() => this.appointmentService.registration(appointment))
    )
    .subscribe(() => {
      this.snackbar.open('Your registration has been toggled.');
      this.loadAppointments();
    }, error => this.store.dispatch(new ThrowError({ error }))
  );
  }

  /**
   * Admin can remove registered appointments
   */
  removeRegistration(evt, appointment) {
    evt.preventDefault();

    this.admin$.pipe(
      take(1),
      filter(val => !!val),
      concatMap(() => this.dialog.confirmDelete()),
      filter(val => !!val),
      concatMap(() => this.appointmentService.deleteRegistration(appointment))
    ).subscribe(
      () => {
        this.snackbar.open('The registration has been removed.');
        this.loadAppointments();
      },
      error => this.store.dispatch(new ThrowError({ error }))
    );
  }

  getWeekday(weekday: number): string {
    return moment.weekdays()[weekday] || '';
  }

  getCountdownForOwnAppointment(): string {
    if (!this.ownAppointment) {
      return '';
    }

    const diffMins = moment(this.ownAppointment['nextDate']).diff(moment(), 'minutes');

    if (diffMins === 0 || diffMins >= (60 * 24 * 7 - 5)) {
      return 'now';
    }

    const mins = diffMins % 60;
    let hours = Math.floor(diffMins / 60);
    const days = Math.floor(hours / 24);
    hours %= 24;

    return 'in ' + [`${days} day(s)`, `${hours} hour(s)`, `${mins} minute(s)`]
      .filter(x => !x.startsWith('0'))
      .join(', ');
  }

  getButtonColor(tab: string) {
    return this.currentTab === tab ? 'primary' : '';
  }

  isCurrentTab(tab: string): boolean {
    return this.currentTab === tab;
  }

  ngOnInit() {
    this.loadAppointments();

    this.currentTab = window.innerWidth > 800 ? 'table' : 'list';

    // initialize websocket for instant data
    this.appointmentSocket = this.appointmentService.getSocket()
      .subscribe(() => {
        this.loadAppointments();
      });

    this.userId$.pipe(
      take(1),
      concatMap(id => this.userService.getProfile(id))
    )
    .subscribe(
      res => {
        this.profile = res;
        this.profileTz = res.timezone && moment.tz.zone(res.timezone)
          ? res.timezone
          : '';
        this.getHoursForLocalTime();
      },
      err => console.log(err)
    );
  }

  ngOnDestroy() {
    this.appointmentSocket.unsubscribe();

    if (this.checkMeetingInterval) {
      this.checkMeetingInterval.unsubscribe();
    }
  }
}
