import { Component } from '@angular/core';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { UserService } from '../../user';
import { debounceTime, map, tap, filter, concatMap } from 'rxjs/operators';
import { DialogService } from '../../dialog/dialog.service';
import { Store } from '@ngrx/store';
import { AppState } from '../../reducers';
import { MatSnackBar } from '@angular/material';
import { ThrowError } from '../../actions/global.actions';

@Component({
  selector: 'user-admin',
  templateUrl: './user-admin.component.html',
  styleUrls: [
    './user-admin.component.styl'
  ]
})
export class UserAdminComponent {

  users: Object[] = [];
  currentSearch = '';
  form: FormGroup;
  search: FormControl = new FormControl('');
  loading = false;
  searched = false;

  constructor(
    private userService: UserService,
    private dialog: DialogService,
    private snackbar: MatSnackBar,
    private store: Store<AppState>,
    fb: FormBuilder
  ) {
    this.form = fb.group({
      'search': this.search
    });
    this.search.valueChanges
      .pipe(
        debounceTime(400),
        map(val => val.trim()),
        tap(val => this.currentSearch = val),
        filter(val => !!val)
      )
      .subscribe(() => this.loadUsers());
  }

  /**
   * Loads all users
   */
  loadUsers() {
    this.loading = true;
    this.userService
      .search(this.currentSearch)
      .subscribe(res => {
        this.users = res;
        this.loading = false;
        this.searched = true;
      }, error => this.store.dispatch(new ThrowError({ error })));
  }

  delete(evt, user) {
    evt.preventDefault();

    this.dialog.confirmDelete().pipe(
      filter(val => !!val),
      concatMap(() => this.userService.delete(user))
    ).subscribe(
      () => {
        this.snackbar.open('The user has been deleted successfully.');
        this.loadUsers();
      },
      error => this.store.dispatch(new ThrowError({ error }))
    );
  }
}
